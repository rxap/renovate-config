# RxAP Renovate configuration presets

## Default

- Extend the base configuration `config:base`
- Enable lock file maintenance
- Enable GitLab automerge
- Automerge all pin and patch dependencies updates
- Automerge all pin, patch and minor devDependencies updates
- Labling update types and dep types with corsponding labels

### Usage

###### .renovaterc.json
```json
{
  "extends": [
    "gitlab>rxap/renovate-config"
  ]
}
```
